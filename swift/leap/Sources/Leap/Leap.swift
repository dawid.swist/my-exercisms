//Solution goes in Sources

struct Year {

    private let year:Int;

    let isLeapYear: Bool;

    init(calendarYear: Int) {
        self.year = calendarYear
        self.isLeapYear = Year.isDivisibleByX(calendarYear,4) && Year.isSomeLeapDivisionExceptions(calendarYear)
    }

    static private func isSomeLeapDivisionExceptions(_ year:Int) -> Bool {

        let isDivisionBy100 = Year.isDivisibleByX(year, 100)
        return !isDivisionBy100 || isDivisibleByX(year, 400)
    }

    static private func isDivisibleByX(_ value:Int, _   x:Int) -> Bool {
        return (value % x) == 0
    }
}
