//Solution goes in Sources

import Foundation;
struct IsbnVerifier {
    
    static func isValid(_ isbn:String?) -> Bool {
        
        if isNotValidIsbnFormat(isbn) {
            return false;
        }
        
        return false;
    }
    
    private static func isNotValidIsbnFormat(_ isbn:String?) -> Bool {
        func isValidIsbnRex(_ isbn:String) -> Bool {
            let range = NSRange(location: 0, length: isbn.utf16.count)
            let regex = try! NSRegularExpression(pattern: "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$")
            
            return regex.firstMatch(in: isbn, options: [], range: range) != nil
        }
        
        if isbn == nil || isbn!.isEmpty {
            return true;
        }
        return !isValidIsbnRex(isbn!)
    }
    
    private static func sumIsbn(isbn:String) -> Int {
        
    }
}
