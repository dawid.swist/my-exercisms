//Solution goes in Sources

func hello() -> String {
    return "Hello, World!"
}

func hello(_ msg:String) -> String {
    
    return "Hello, \(msg)!"
}
