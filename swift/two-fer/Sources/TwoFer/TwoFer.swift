


    //One for you, one for me.

    func twoFer(name: String = "you") -> String {
        return "One for \(name), one for me."
    }
