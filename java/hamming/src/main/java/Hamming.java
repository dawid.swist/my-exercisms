class Hamming {

    private final String leftStrand;

    private final String rightStrand;
    private final int hammingDistance;

    Hamming(String leftStrand, String rightStrand) {

        validateInputCheck(leftStrand, rightStrand);

        this.leftStrand = leftStrand;
        this.rightStrand = rightStrand;

        this.hammingDistance = calculateHammingDistance();

    }

    private void validateInputCheck(String leftStrand, String rightStrand) {
        checkIfFirstStringIsEmpty(leftStrand, rightStrand, "left strand must not be empty.");
        checkIfFirstStringIsEmpty(rightStrand, leftStrand, "right strand must not be empty.");

        if (leftStrand.length() != rightStrand.length()) {
            throw new IllegalArgumentException("leftStrand and rightStrand must be of equal length.");
        }
    }

    private void checkIfFirstStringIsEmpty(String firstStrand, String secondStrand, String errorMsg) {
        if (firstStrand.isEmpty() && !secondStrand.isEmpty()) {
            throw new IllegalArgumentException(errorMsg);
        }
    }

    private int calculateHammingDistance() {
        int distanceSum = 0;

        for (int i = 0; i < leftStrand.length(); i++) {
            if (leftStrand.charAt(i) != rightStrand.charAt(i)) distanceSum++;
        }

        return distanceSum;
    }

    int getHammingDistance() {

        return this.hammingDistance;
    }
}
